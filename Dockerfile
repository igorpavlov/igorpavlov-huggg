FROM node:20.14.0-alpine
RUN mkdir -p /home/node/app/node_modules && chown -R node:node /home/node/app
WORKDIR /home/node/app
USER node
COPY --chown=node:node . .
RUN yarn && yarn build
EXPOSE 8080
ENV HOST 0.0.0.0
CMD [ "yarn", "start"]
