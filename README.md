# Build & Run

The easiest way to run the app is with Docker.

```
sudo docker build -t igorpavlov-huggg-task . && docker run -ti -p 8080:8080 igorpavlov-huggg-task
```

# Live Test

```
curl http://localhost:8080/api/brand/5a4e6d14-53d4-4583-bd6b-49f81b021d24
curl http://localhost:8080/api/product/all/bybrand/57304d7b-a233-4952-8a91-3cbb7870be30
curl http://localhost:8080/api/store/all/bybrand/57304d7b-a233-4952-8a91-3cbb7870be30
curl http://localhost:8080/api/store/all/byproduct/26f7a82a-30a8-44e4-93cb-499a256d0ce9
```
