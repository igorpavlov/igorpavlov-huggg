import Fastify from "fastify";
import { BrandType } from "../types/brand";
import { Data } from "./data.class";
import { ProductType } from "src/types/product";
export class Brand {
  static findById(brandId: BrandType["id"]): BrandType {
    const data = new Data().getData();

    const brand = data.data.find((brand) => brand.id === brandId);

    if (brand == null) {
      throw new Fastify.errorCodes.FST_ERR_NOT_FOUND();
    }

    return brand;
  }

  static findByProductId(productId: ProductType["id"]): BrandType[] {
    const data = new Data().getData();

    const brands = data.data.filter((brand) =>
      [...brand.products, ...brand.consolidated_products].includes(productId),
    );

    return brands;
  }
}
