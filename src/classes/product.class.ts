import { BrandType } from "../types/brand";
import { Data } from "./data.class";
import { Brand } from "./brand.class";
import { ProductType } from "src/types/product";

export class Product {
  static findByBrandId(brandId: BrandType["id"]): ProductType[] {
    const data = new Data().getData();
    const brand = Brand.findById(brandId);
    const productIds = [...brand.products, ...brand.consolidated_products];
    const products = data.embedded.products.filter((product) =>
      productIds.includes(product.id),
    );

    return products;
  }
}
