// Can be replaced with, for example, MongoDB
import { DataType } from "../types/data";
import mockedData from "../../mocks/brands.json";

export class Data {
  mockedData: DataType;

  constructor() {
    this.mockedData = mockedData;
  }

  getData() {
    return this.mockedData;
  }
}
