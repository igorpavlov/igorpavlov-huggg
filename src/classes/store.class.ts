import { BrandType } from "../types/brand";
import { ProductType } from "../types/product";
import { Data } from "./data.class";
import { Brand } from "./brand.class";
import { StoreType } from "src/types/store";

export class Store {
  static findByBrandId(brandId: BrandType["id"]): StoreType[] {
    const data = new Data().getData();
    const brand = Brand.findById(brandId);
    const storeIds = brand.stores;
    const stores = data.embedded.stores.filter((store) =>
      storeIds.includes(store.id),
    );

    return stores;
  }

  private static findByIds(storeIds: StoreType["id"][]): StoreType[] {
    const data = new Data().getData();

    const stores = data.embedded.stores.filter((store) =>
      storeIds.includes(store.id),
    );

    return stores;
  }

  static findByProductId(productId: ProductType["id"]): StoreType[] {
    const brands = Brand.findByProductId(productId);

    const storeIds = brands.reduce(
      (storeIdsArr, brand) => [...storeIdsArr, ...brand.stores],
      <StoreType["id"][]>[],
    );
    const stores = Store.findByIds(storeIds);

    return stores;
  }
}
