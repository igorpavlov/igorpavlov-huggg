import { FastifyInstance } from "fastify";
import { BrandType } from "../types/brand";
import { Store } from "../classes/store.class";
import { StoreType } from "src/types/store";
import { ProductType } from "src/types/product";

export default async function store(server: FastifyInstance) {
  server.get<{ Params: { brandId: BrandType["id"] }; Reply: StoreType[] }>(
    "/api/store/all/bybrand/:brandId",
    async (request) => {
      const { brandId } = request.params;

      return Store.findByBrandId(brandId);
    },
  );

  server.get<{ Params: { productId: ProductType["id"] }; Reply: StoreType[] }>(
    "/api/store/all/byproduct/:productId",
    async (request) => {
      const { productId } = request.params;

      return Store.findByProductId(productId);
    },
  );
}
