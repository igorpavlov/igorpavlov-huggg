import { FastifyInstance } from "fastify";
import { BrandType } from "../types/brand";
import { Brand } from "../classes/brand.class";

export default async function (server: FastifyInstance) {
  server.get<{ Params: { brandId: BrandType["id"] }; Reply: BrandType }>(
    "/api/brand/:brandId",
    async (request) => {
      const { brandId } = request.params;

      return Brand.findById(brandId);
    },
  );
}
