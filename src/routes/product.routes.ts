import { FastifyInstance } from "fastify";
import { BrandType } from "../types/brand";
import { Product } from "../classes/product.class";
import { ProductType } from "src/types/product";

export default async function product(server: FastifyInstance) {
  server.get<{ Params: { brandId: BrandType["id"] }; Reply: ProductType[] }>(
    "/api/product/all/bybrand/:brandId",
    async (request) => {
      const { brandId } = request.params;

      return Product.findByBrandId(brandId);
    },
  );
}
