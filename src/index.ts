import Fastify from "fastify";

import BrandRoutes from "./routes/brand.routes";
import ProductRoutes from "./routes/product.routes";
import StoreRoutes from "./routes/store.routes";

const server = Fastify();

server.listen({ port: 8080, host: process.env.HOST || "localhost" });

server.register(BrandRoutes);
server.register(ProductRoutes);
server.register(StoreRoutes);
