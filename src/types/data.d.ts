import { Static, Type } from "@sinclair/typebox";
import { Brand } from "./brand";
import { Product } from "./product";
import { Store } from "./store";

export const Data = Type.Object({
  current_page: Type.Number(),
  data: Type.Array(Brand),
  embedded: Type.Object({
    products: Type.Array(Product),
    stores: Type.Array(Store),
  }),
  from: Type.Number(),
  last_page: Type.Number(),
  next_page_url: Type.String({ format: "url" }),
  path: Type.String({ format: "url" }),
  per_page: Type.Number(),
  prev_page_url: Type.Union([Type.Null(), Type.String()]),
  to: Type.Number(),
  total: Type.Number(),
});

export type DataType = Static<typeof Data>;
