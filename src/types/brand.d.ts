import { Static, Type } from "@sinclair/typebox";

export const Brand = Type.Object({
  id: Type.String({ format: "uuid" }),
  created_at: Type.String({ format: "date-time" }),
  updated_at: Type.String({ format: "date-time" }),
  name: Type.String(),
  internal_name: Type.String(),
  logo: Type.String(),
  colour: Type.String(),
  success: Type.String(),
  share: Type.String(),
  weight: Type.Number(),
  deleted_at: Type.Union([Type.Null(), Type.String({ format: "date-time" })]),
  expiry: Type.Number(),
  website: Type.Union([Type.Null(), Type.String()]),
  integration_id: Type.Number(),
  user_id: Type.String({ format: "uuid" }),
  email: Type.Union([Type.Null(), Type.String({ format: "email" })]),
  vat: Type.Number(),
  faq: Type.Union([Type.Null(), Type.String()]),
  description: Type.String({ format: "email" }),
  redeem: Type.Union([Type.Null(), Type.String()]),
  location_text: Type.Union([Type.Null(), Type.String()]),
  map_pin_url: Type.String({ format: "uri" }),
  consolidated: Type.Number(),
  default_location_description_markdown: Type.String(),
  products: Type.Array(Type.String({ format: "uuid" })),
  consolidated_products: Type.Array(Type.String({ format: "uuid" })),
  stores: Type.Array(Type.String({ format: "uuid" })),
  logo_url: Type.String({ format: "uri" }),
});

export type BrandType = Static<typeof Brand>;
