import { Static, Type } from "@sinclair/typebox";

export const Store = Type.Object({
  id: Type.String({ format: "uuid" }),
  brand_id: Type.String({ format: "uuid" }),
  latitiude: Type.String(),
  longitude: Type.String(),
  website: Type.Union([Type.Null(), Type.String()]),
  name: Type.String(),
  description: Type.String(),
  visible: Type.Number(),
  description_markdown: Type.String(),
  image: Type.String(),
  image_url: Type.String({ format: "uri" }),
  latitude: Type.String(),
});

export type StoreType = Static<typeof Store>;
