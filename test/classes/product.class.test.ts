import { describe, expect, test } from "@jest/globals";
import { Product } from "../../src/classes/product.class";

describe("Product", () => {
  describe("Get all by brand ID", () => {
    test("finds by existent brand ID including consolidated", () => {
      const brandId = "a715b837-f4fc-48ba-ba0a-7f53b6dc59c5";
      const productIds = [
        "26f7a82a-30a8-44e4-93cb-499a256d0ce9",
        "f5c72f41-972d-42b6-9ac5-51bad2afd01f",
        "57186a73-7857-4684-bf82-b2bc7b8a1040",
        "84fa5849-472e-4625-ab86-8f4540138363",
      ];

      const products = Product.findByBrandId(brandId);

      expect(products.map((product) => product.id)).toEqual(productIds);
    });
  });
});
