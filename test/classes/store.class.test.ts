import { describe, expect, test } from "@jest/globals";
import { Store } from "../../src/classes/store.class";

describe("Store", () => {
  describe("Get all by brand ID", () => {
    test("finds by existent brand ID", () => {
      const brandId = "1bb152d8-e912-46d4-a40d-c4eeeeb3cca0";
      const storeIds = [
        "26fe095b-e759-4026-bcc2-9ef046e65416",
        "4ffec9a1-7cdc-444c-982f-0fda2f7d0d01",
      ];

      const stores = Store.findByBrandId(brandId);

      expect(stores.map((store) => store.id)).toEqual(storeIds);
    });
  });

  describe("Get all by product ID", () => {
    test("finds by existent product ID", () => {
      const productId = "0e52f954-b3f2-4509-bd91-c60d15f79e85";
      const storesIds = [
        "26d3b556-0160-48b2-99e5-d313e46f5ef0",
        "892beff3-c85b-44ba-855c-e2066f23a7fa",
      ];

      const stores = Store.findByProductId(productId);

      expect(stores.map((store) => store.id)).toEqual(storesIds);
    });

    test("finds by existent consolidated product ID", () => {
      const productId = "26f7a82a-30a8-44e4-93cb-499a256d0ce9";

      const stores = Store.findByProductId(productId);

      expect(stores.length).toEqual(46);
      expect(stores[0].id).toEqual("120cad4a-d5ed-4e69-9619-193943518a64");
      expect(stores[20].id).toEqual("820d2c40-8ee1-4853-9865-45edaee1f1c8");
      expect(stores[45].id).toEqual("fa8eda05-5acb-48d8-9cb3-3feb3da6241a");
    });
  });
});
