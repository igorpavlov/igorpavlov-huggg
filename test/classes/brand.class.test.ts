import { describe, expect, test } from "@jest/globals";
import Fastify from "fastify";
import { Brand } from "../../src/classes/brand.class";
import mockedData from "../../mocks/brands.json";

describe("Brand", () => {
  describe("Get by brand ID", () => {
    test("finds by existent ID", () => {
      const testedIndex = 3;

      const brand = Brand.findById(mockedData.data[testedIndex].id);

      expect(brand).toBe(mockedData.data[testedIndex]);
    });

    test("does not find by non existent ID", () => {
      const nonExistentId = "5555555-aaaa-bbbb-bbbb-111111111111";

      expect(() => Brand.findById(nonExistentId)).toThrow(
        Fastify.errorCodes.FST_ERR_NOT_FOUND,
      );
    });
  });

  describe("Get by product ID", () => {
    test("finds by product ID", () => {
      const productId = "6a46ea89-da25-456e-a563-c1726e425099";

      const brands = Brand.findByProductId(productId);

      expect(brands.length).toBe(1);
      expect(brands[0].id).toBe("1b9f5fae-4e2e-429c-9372-940022473129");
    });

    test("finds by consolidated product ID", () => {
      const productId = "26f7a82a-30a8-44e4-93cb-499a256d0ce9";

      const brands = Brand.findByProductId(productId);

      expect(brands.map((brand) => brand.id)).toEqual([
        "66462cd6-e43c-4ab6-8e6f-004ca189e4b9",
        "a715b837-f4fc-48ba-ba0a-7f53b6dc59c5",
        "69be9b8c-5b95-4792-a05c-652d2f15a62f",
        "1f93dfab-8c4e-405a-95cc-c14c01a68773",
        "160edae7-e35c-443b-80f4-88bfd7d171d5",
        "15538f17-95bd-4cc4-9cf3-893a21d16028",
        "59f14fdc-7db6-41d2-a2af-56357801f3ef",
        "6fa700c8-4367-43b0-8b79-dc6f2e58901b",
        "9e225078-d157-4402-8590-60df83de40d6",
        "32111cd0-db1d-4314-bfd2-619421249b41",
        "1bb152d8-e912-46d4-a40d-c4eeeeb3cca0",
        "1b9f5fae-4e2e-429c-9372-940022473129",
        "01c25854-6b19-4494-be81-777284b34d2f",
      ]);
    });
  });
});
