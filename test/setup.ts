import { Data } from "../src/classes/data.class";
import mockedData from "../mocks/brands.json";

global.beforeAll(() => {
  jest.spyOn(Data.prototype, "getData").mockImplementation(() => mockedData);
});

global.afterAll(() => {
  jest.restoreAllMocks();
});
