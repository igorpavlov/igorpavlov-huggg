import { describe, expect, test } from "@jest/globals";
import { DataType } from "../../src/types/data";
import { BrandType } from "../../src/types/brand";
import { ProductType } from "../../src/types/product";
import { StoreType } from "../../src/types/store";
import dataMocked from "../../mocks/brands.json";

describe("Types", () => {
  test("Data type is defined correctly based on the mock data", () =>
    expect(() => dataMocked as DataType).not.toThrow());

  test("Brand type is defined correctly based on the mock data", () =>
    expect(() => dataMocked.data as BrandType[]).not.toThrow());

  test("Product type is defined correctly based on the mock data", () =>
    expect(() => dataMocked.embedded.products as ProductType[]).not.toThrow());

  test("Store type is defined correctly based on the mock data", () =>
    expect(() => dataMocked.embedded.stores as StoreType[]).not.toThrow());
});
